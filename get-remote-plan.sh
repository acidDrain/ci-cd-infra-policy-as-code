#!/bin/sh

# set -Eeuo pipefail
set -eu

DEBUG="${DEBUG:-}"

#----------------------------------------------------------#
#  If DEBUG Environment Variable Set Turn On Command Echo  #
#----------------------------------------------------------#
if [ -n "$DEBUG" ]; then
  set -x
fi

#----------------------------------------------------------#
#              Retrieve Environment Variables              #
#----------------------------------------------------------#
if [ -e .env ]; then
  . ./.env
fi

WORKING_DIR="$( cd "$( dirname "$0" )" && pwd )"
PLAN_DIR="${WORKING_DIR}/plans"
TF_CLOUD_TOKEN="${TF_CLOUD_TOKEN:-}"
BASE_URL="${BASE_URL:-"https://app.terraform.io"}"
ORG_NAME="${ORG_NAME:-""}"
WORKSPACE_NAME="${WORKSPACE_NAME:-""}"

if [ ! -d "${PLAN_DIR}" ]; then
  mkdir -p "${PLAN_DIR}"
fi
#----------------------------------------------------------#
#                    Get Workspace Name                    #
#----------------------------------------------------------#
#WORKSPACE_NAME="$(curl -sSL \
#  --header "Authorization: Bearer $TF_CLOUD_TOKEN" \
#  --header "Content-Type: application/vnd.api+json" \
#  "https://app.terraform.io/api/v2/organizations/${ORG_NAME}/workspaces" | jq -M -r '.data[0].attributes.name')"
#----------------------------------------------------------#
#                     Get Latest Run ID                    #
#----------------------------------------------------------#
RUN_ID="$(curl -sSL \
  --header "Authorization: Bearer $TF_CLOUD_TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  "https://app.terraform.io/api/v2/workspaces/${WORKSPACE_NAME}/runs" | jq -M -r '.data[0].id')"

#----------------------------------------------------------#
#                       Get the Plan                       #
#----------------------------------------------------------#
curl \
  -sSL \
  --header "Authorization: Bearer $TF_CLOUD_TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  "${BASE_URL}/api/v2/runs/${RUN_ID}/plan/json-output" | jq -M '' > "${PLAN_DIR}/tfplan.json" && echo "Success - saved plan to ${PLAN_DIR}/tfplan.json"  && exit 0

