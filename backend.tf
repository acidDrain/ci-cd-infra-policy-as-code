terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "ued"

    workspaces {
      name = "ci-cd-infra-policy-as-code"
    }
  }
}
