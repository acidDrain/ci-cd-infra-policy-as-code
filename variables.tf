variable "aws_access_key" {
  description = "AWS Access Key for Terraform"
  type        = string
  sensitive   = true
}

variable "aws_secret_access_key" {
  description = "AWS Secret Access Key for Terraform"
  type        = string
  sensitive   = true
}

variable "region" {
  description = "AWS Region to target the deployment"
  type        = string
  sensitive   = false
}

